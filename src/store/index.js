import axios from 'axios';
import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex, axios);

export default new Vuex.Store({
    state: {
        posts: [],
        post: {}
    },
    actions: {
        getAllPosts ({commit}) {
            axios
            .get('http://localhost:3000/posts')
            .then(response => {
                commit('loadData', response.data)
            })
        },
        getPostById ({commit}, id) {
            axios
            .get(`http://localhost:3000/posts/${id}`)
            .then(response => {
                commit('loadPost', response.data)
            })
        }
    },
    mutations: {
        loadData(state,posts) {
            state.posts = posts
        },
        loadPost(state, post) {
            state.post = post
        }
    }
});