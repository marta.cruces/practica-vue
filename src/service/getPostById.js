import * as axios from 'axios';

async function getPostById (id) {
    const result = await axios.get(`http://localhost:3000/posts/${id}`);
    return result.data;
}

export default getPostById;