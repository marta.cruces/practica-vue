import * as axios from 'axios';

async function getAllPosts() {
    const result = await axios.get('http://localhost:3000/posts');
    return result.data;
}

export default getAllPosts;